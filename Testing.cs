using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Testing
    {
        private string v1;
        private string v2;
        private List<string> list;

        public string idq { get; private set; }
        public string question { get; private set; }
        public string correctAnswer { get; private set; }
        public List<string> answers { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quest"></param>
        /// <param name="cAnswer"></param>
        /// <param name="answ"></param>
        public Testing( string quest, string cAnswer, List<string> answ)
        {
            // idq = id;
            question = quest;
            correctAnswer = cAnswer;
            answers = answ;
        }

        
    }
}
