using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data.SqlClient;

namespace Test
{
    public partial class Form1 : Form
    {
        int rightAnswer; // Номер правильного ответа    
        int Check;  // Количество правильных ответов
        int Choice; // Номер выбранного ответа
        int QuestionNumber = 1; // Номер вопроса
        const string name = "Test";

        //public List<Testing> myClassArray { get; private set; }

        // ВРЕМЕННЫЙ КОСТЫЛЬ ДО ПОЯВЛЕНИЯ БД
        Question Q1 = new Question("Вопрос 1", "Ответ 1", "Ответ 2", "Ответ 3", 1); // Вопрос 1
        Question Q2 = new Question("Вопрос 2", "Ответ 1", "Ответ 2", "Ответ 3", 2); // Вопрос 2
        Question Q3 = new Question("Вопрос 3", "Ответ 1", "Ответ 2", "Ответ 3", 3); // Вопрос 3

        private void Form1_Load(object sender, EventArgs e)
        {
            label3.Text = Environment.UserName;
            // Подписка на событие изменение состояния переключателей RadioButton:
            radioButton1.CheckedChanged += new EventHandler(ChangeStatus);
            radioButton2.CheckedChanged += new EventHandler(ChangeStatus);
            radioButton3.CheckedChanged += new EventHandler(ChangeStatus);
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            label1.Visible = false;
            button2.Visible = true;
            label2.Visible = true;
            radioButton1.Visible = true;
            radioButton2.Visible = true;
            radioButton3.Visible = true;
            StartTest();
        }
        void ChangeStatus(Object sender, EventArgs e)
        {
            // Кнопка "Продолжить" становится активной, и ей передаем фокус:
            button2.Enabled = true;
            button2.Focus();
            RadioButton Switch = (RadioButton)sender;
            var tmp = Switch.Name;
            // Номер выбранного ответа
            Choice = int.Parse(tmp.Substring(11));
        }
        void StartTest()
        {
            switch (QuestionNumber)
            {
                case 1:
                    label2.Text = Q1.QuestionText;
                    radioButton1.Text = Q1.Answer1;
                    radioButton2.Text = Q1.Answer2;
                    radioButton3.Text = Q1.Answer3;
                    rightAnswer = Q1.CorrectAnswer;
                    break;
                case 2:
                    label2.Text = Q2.QuestionText;
                    radioButton1.Text = Q2.Answer1;
                    radioButton2.Text = Q2.Answer2;
                    radioButton3.Text = Q2.Answer3;
                    rightAnswer = Q2.CorrectAnswer;
                    break;
                case 3:
                    label2.Text = Q3.QuestionText;
                    radioButton1.Text = Q3.Answer1;
                    radioButton2.Text = Q3.Answer2;
                    radioButton3.Text = Q3.Answer3;
                    rightAnswer = Q3.CorrectAnswer;
                    break;
            }
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            button2.Enabled = false;
        }

        private void buttonCont_Click(object sender, EventArgs e)
        {
            if (Choice == rightAnswer) 
            {
                Check++;
                if (QuestionNumber == 3 && Check == 3)
                {
                    string message = "Вы успешно прошли тест!";
                    string caption = "Успех!";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;
                    result = MessageBox.Show(message, caption, buttons);

                    Application.Exit();
                }
                QuestionNumber++;
            }

            if (Choice != rightAnswer) 
            {
                string message = "Вы не справились, обратитесь к руководству!";
                string caption = "Ошибка!";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
                radioButton1.Visible = false;
                radioButton2.Visible = false;
                radioButton3.Visible = false;
                label1.Visible = true;
                label2.Visible = false;
                button2.Visible = false;
                label1.Text = "Неудача! Для продолжения работы обратитесь к руководству!";
            }
            StartTest();
        }
        // Запрещаем пользователю закрытие формы
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
                e.Cancel = true;
        }
      
        public Form1()
        {
            //SqlConnection conn = new SqlConnection(/*"connection string"*/);
            //try
            //{
            //  conn.Open();
            //  SqlDataAdapter adapter = new SqlDataAdapter(/*"command"*/, conn);
            //  SqlCommandBuilder builder = new SqlCommandBuilder(adapter);
            //  DataSet ds = new DataSet();
            //  adapter.Fill(ds);
            //  DataTable table = ds.Tables["Questions"];
            //  myClassArray = new List<Testing>();
            //  for (int i = 0; i < table.Rows.Count; i++)
            //  {
            //      Testing buf = new Testing(table.Rows[i][0].ToString(), table.Rows[i][1].ToString(),
            //          new List<string> {
            //              table.Rows[i][2].ToString(),
            //              table.Rows[i][3].ToString(),
            //              table.Rows[i][4].ToString(),
            //              table.Rows[i][5].ToString()
            //          });
            //      myClassArray.Add(buf);
            //  }
            //  //в коллекции хранятся классы, которые соотвествуют вопросам
            //}
            //catch{ }

            InitializeComponent();
        }

    }
}
