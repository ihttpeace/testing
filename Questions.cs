using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Question
    {
        public string QuestionText { get; set; }

        public string Answer1 { get; set; } 
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        private int _correctAnswer;

        public int CorrectAnswer
        {
            get { return _correctAnswer; }
            set { _correctAnswer = value; }
        }
        /// <summary>
        /// Вопрос
        /// </summary>
        /// <param name="questionText">Текст вопроса</param>
        /// <param name="answer1">Ответ 1</param>
        /// <param name="answer2">Ответ 2</param>
        /// <param name="answer3">Ответ 3</param>
        /// <param name="correctAnswer">Номер правильного ответа</param>
        public Question(string questionText, string answer1, string answer2, string answer3, int correctAnswer)
        {
            this.QuestionText = questionText;
            this.Answer1 = answer1;
            this.Answer2 = answer2;
            this.Answer3 = answer3;
            CorrectAnswer = correctAnswer;
        }
    }
}
